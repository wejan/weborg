;;; ox-config.el --- HTML Back-End for Org Export Engine -*- lexical-binding: t; -*-
;; Modified from ox-html.el
(require 'cl-lib)
(require 'format-spec)
(require 'ox)
(require 'ox-publish)
(require 'table)

(org-export-define-backend 'xhtml 'html
  '((template . org-xhtml-template))
  :filters-alist '((:filter-options . org-html-infojs-install-script)
		   (:filter-parse-tree . org-html-image-link-filter)
		   (:filter-final-output . org-html-final-function))
  :options-alist
  '((:xhtml-divs nil nil org-xhtml-divs)
    (:comment nil "comment" org-comment)
    (:entrymeta nil "entrymeta" org-entrymeta)
    (:pagenav nil "pagenav" org-pagenav)
    (:comment-format nil nil org-comment-format)
    (:entrymeta-format nil nil org-entrymeta-format)))

(defcustom org-xhtml-divs
  '((preamble  "div" "preamble")
    (content   "div" "content")
    (postamble "div" "postamble")
    (comment "div" "comment")
    (entrymeta "div" "entrymeta"))
  "Alist of the three section elements for HTML export.
The car of each entry is one of `preamble', `content' or `postamble'.
The cdrs of each entry are the ELEMENT_TYPE and ID for each
section of the exported document. ;; Add `comment', `entry-meta'.

Note that changing the default will prevent you from using
org-info.js for your website."
  :group 'org-export-html
  :version "24.4"
  :package-version '(Org . "8.0")
  :type '(list :greedy t
	       (list :tag "Preamble"
		     (const :format "" preamble)
		     (string :tag "element") (string :tag "     id"))
	       (list :tag "Content"
		     (const :format "" content)
		     (string :tag "element") (string :tag "     id"))
	       (list :tag "Comment"
		     (const :format "" comment)
		     (string :tag "element") (string :tag "     id"))
	       (list :tag "Entrymeta"
		     (const :format "" entrymeta)
		     (string :tag "element") (string :tag "     id"))
	       (list :tag "Postamble" (const :format "" postamble)
		     (string :tag "     id") (string :tag "element"))))

;;;; Template :: Entry-meta  ;; modified postamble

(defcustom org-entrymeta nil
  "Non-nil means insert a entry-meta in HTML export.

When t, insert a string as defined by the formatting string in
`org-html-entry-meta-format'.  When set to a string, use this
formatting string instead (see `org-html-postamble-format' for an
example of such a formatting string).

When set to a function, apply this function and insert the
returned string.  The function takes the property list of export
options as its only argument.

Setting :html-postamble in publishing projects will take
precedence over this variable."
  :group 'org-export-html
  :type '(choice (const :tag "No entry-meta" nil)
		 (const :tag "Auto entry-meta" auto)
		 (const :tag "Default formatting string" t)
		 (string :tag "Custom formatting string")
		 (function :tag "Function (must return a string)")))

(defcustom org-entrymeta-format
  '(("en" "<span class=\"author\">%a</span> &bull;
<span class=\"date\">%d</span> &bull;
<span class=\"date\">Last update: %C</span>"))
  "Alist of languages and format strings for the HTML postamble.

The first element of each list is the language code, as used for
the LANGUAGE keyword.  See `org-export-default-language'.

The second element of each list is a format string to format the
postamble itself.  This format string can contain these elements:

  %t stands for the title.
  %s stands for the subtitle.
  %a stands for the author's name.
  %e stands for the author's email.
  %d stands for the date.
  %c will be replaced by `org-html-creator-string'.
  %v will be replaced by `org-html-validation-link'.
  %T will be replaced by the export time.
  %C will be replaced by the last modification time.

If you need to use a \"%\" character, you need to escape it
like that: \"%%\"."
  :group 'org-export-html
  :type '(repeat
	  (list (string :tag "Language")
		(string :tag "Format string"))))

;;;; Template :: Comment  ;; modified Postamble

(defcustom org-comment nil
  "Non-nil means insert a comment in HTML export.

When t, insert a string as defined by the formatting string in
`org-html-comment-format'.  When set to a string, use this
formatting string instead (see `org-html-postamble-format' for an
example of such a formatting string).

When set to a function, apply this function and insert the
returned string.  The function takes the property list of export
options as its only argument.

Setting :html-postamble in publishing projects will take
precedence over this variable."
  :group 'org-export-html
  :type '(choice (const :tag "No comment" nil)
		 (const :tag "Default formatting string" t)
		 (string :tag "Custom formatting string")
		 (function :tag "Function (must return a string)")))

(defcustom org-comment-format
  '(("en" "<h2 class=\"comment\">Comments:</h2>
<p>Email questions, comments, and corrections to my email.</p>"))
  "Alist of languages and format strings for the HTML postamble.

The first element of each list is the language code, as used for
the LANGUAGE keyword.  See `org-export-default-language'."
  :group 'org-export-html
  :type '(repeat
	  (list (string :tag "Language")
		(string :tag "Format string"))))

;; Template ; Page Navigation, post prev/next
(defcustom org-pagenav t
  "Non-nil means insert a page navigation to post-entry in HTML export."
  :group 'org-export-html
  :version "24.4"
  :package-version '(Org . "8.0")
  :type 'boolean)

(defun org-xhtml--build-meta-info (info)
  "Return meta tags for exported document.
INFO is a plist used as a communication channel."
  (let* ((title (org-html-plain-text
		 (org-element-interpret-data (plist-get info :title)) info))
	 ;; Set title to an invisible character instead of leaving it
	 ;; empty, which is invalid.
	 (title (if (org-string-nw-p title) title "&lrm;"))
	 (charset (or (and org-html-coding-system
			   (fboundp 'coding-system-get)
			   (symbol-name
			    (coding-system-get org-html-coding-system
					       'mime-charset)))
		      "iso-8859-1")))
    (concat
     (when (plist-get info :time-stamp-file)
       (format-time-string
	(concat "<!-- "
		(plist-get info :html-metadata-timestamp-format)
		" -->\n")))

     (if (org-html-html5-p info)
	 (org-html--build-meta-entry "charset" charset)
       (org-html--build-meta-entry "http-equiv" "Content-Type"
				   (concat "text/html;charset=" charset)))

     (let ((viewport-options
	    (cl-remove-if-not (lambda (cell) (org-string-nw-p (cadr cell)))
			      (plist-get info :html-viewport))))
       (if viewport-options
	   (org-html--build-meta-entry "name" "viewport"
				       (mapconcat
					(lambda (elm)
                                          (format "%s=%s" (car elm) (cadr elm)))
					viewport-options ", "))))
     (if sitename
       (if (equal sitename title)
	   (format "<title>%s</title>\n" title)
	   (if (equal "&lrm;" title)
	      (format "<title>%s</title>\n" sitename)
       (format "<title>%s - %s</title>\n" title sitename)))
       (format "<title>%s</title>\n" title))

     (mapconcat
      (lambda (args) (apply #'org-html--build-meta-entry args))
      (delq nil (if (functionp org-html-meta-tags)
		    (funcall org-html-meta-tags info)
		  org-html-meta-tags))
      ""))))

(defun org-xhtml--build-pre/postamble (type info)
  "Return document preamble or postamble as a string, or nil.
TYPE is either `preamble' or `postamble', INFO is a plist used as a
communication channel."
  (let ((section (plist-get info (intern (format ":html-%s" type))))
	(spec (org-html-format-spec info)))
    (when section
      (let ((section-contents
	     (if (functionp section) (funcall section info)
	       (cond
		((stringp section) (format-spec section spec))
		((eq section 'auto)
		 (let ((date (cdr (assq ?d spec)))
		       (author (cdr (assq ?a spec)))
		       (email (cdr (assq ?e spec)))
		       (creator (cdr (assq ?c spec)))
		       (validation-link (cdr (assq ?v spec))))
		   (concat
		    (and (plist-get info :with-date)
			 (org-string-nw-p date)
			 (format "<p class=\"date\">%s: %s</p>\n"
				 (org-html--translate "Date" info)
				 date))
		    (and (plist-get info :with-author)
			 (org-string-nw-p author)
			 (format "<p class=\"author\">%s: %s</p>\n"
				 (org-html--translate "Author" info)
				 author))
		    (and (plist-get info :with-email)
			 (org-string-nw-p email)
			 (format "<p class=\"email\">%s: %s</p>\n"
				 (org-html--translate "Email" info)
				 email))
		    (and (plist-get info :time-stamp-file)
			 (format
			  "<p class=\"date\">%s: %s</p>\n"
			  (org-html--translate "Created" info)
			  (format-time-string
			   (plist-get info :html-metadata-timestamp-format))))
		    (and (plist-get info :with-creator)
			 (org-string-nw-p creator)
			 (format "<p class=\"creator\">%s</p>\n" creator))
		    (and (org-string-nw-p validation-link)
			 (format "<p class=\"validation\">%s</p>\n"
				 validation-link)))))
		(t
		 (let ((formats (plist-get info (cond ((eq type 'preamble)
						    :html-preamble-format)
						      ((eq type 'postamble)
						  :html-postamble-format)
						      ((eq type 'comment)
						       :comment-format)
						      ((eq type 'entrymeta)
						       :entrymeta-format))))
		       (language (plist-get info :language)))
		   (format-spec
		    (cadr (or (assoc-string language formats t)
			      (assoc-string "en" formats t)))
		    spec)))))))
	(let ((div (assq type (plist-get info :xhtml-divs))))
	  (when (org-string-nw-p section-contents)
	    (concat
	     (format "<%s id=\"%s\" class=\"%s\">\n"
		     (nth 1 div)
		     (nth 2 div)
		     org-html--pre/postamble-class)
	     (org-element-normalize-string section-contents)
	     (format "</%s>\n" (nth 1 div)))))))))


;; Blog generators; From https://gitlab.com/jgkamat/jgkamat.gitlab.io/blob/master/jgkamat-website.el
(defun org-timestamp-to-str (stamp)
  "Returns string value if org timestamp. Else, return stamp."
  (if (and (not (stringp stamp)) (eql (cl-first stamp) 'timestamp))
      (plist-get (cl-second stamp) ':raw-value)
    stamp))

(defun gen-raw-org-date (filename)
  "Generates raw date headers from filenames"
  (let ((filename (file-relative-name (file-truename filename))))
    (with-temp-buffer
      (insert-file-contents filename)
      (plist-get (org-export-get-environment) ':date))))

(defun gen-org-property (filename)
  "Generates an org property from a filename"
  (let ((filename (file-relative-name (concat (file-name-sans-extension filename) ".html"))))
    (with-temp-buffer
      (insert-file-contents (concat (file-name-sans-extension filename) ".org"))
      ;; TODO use a plist here instead of hacky ordering
      `(,filename ,(plist-get (org-export-get-environment) ':date) ,(plist-get (org-export-get-environment) ':title)))))

(defun gen-links-properties (&optional directory)
  "Gens a sorted (by date) (filename . properties) from an org directory"
  (let* ((directory (or directory (concat ".")))
         (files (directory-files-recursively directory "^.*\.org$")))
    (sort
     ;; Map environments to (filename . property titles)
     (mapcar #'gen-org-property
             (seq-remove
              (lambda (test-file)
                (cl-find-if (lambda (exclude-file)
                              (file-equal-p test-file exclude-file))
                            overview-exclude-files))
              files))
     (lambda (one two)
       (let ((x (org-timestamp-to-str (cl-first (cl-second one))))
             (y (org-timestamp-to-str (cl-first (cl-second two)))))
         ;; (print (concat x " " y))
         (when (and (not (or (eql x nil) (eql y nil)))
                    (or (eql 0 (org-2ft x)) (eql 0 (org-2ft y))))
           (error (concat "Org parsing error found: "
                          x ":" y)))
         (org-time< x y))))))

(defun org-property-to-link (x &optional pre post)
  "Turns a property genrated by gen-org-properties into an org link"
  (let ((pre (or pre "")) (post (or post "")))
    (format "<a href=\"./%s\">%s%s%s</a>" (cl-first x) pre (cl-first (cl-first (last x))) post)))
;; The directory you pass in must be the relative directory to work from (and must be relative to this file)
(defun gen-links (&optional directory)
  "Generates a list of links from a directory"
  (interactive)
  ;; Reduce everything into a string
  (cl-reduce #'concat
             ;; Get desired sorting order
             (reverse
              ;; Map properties to strings
              (mapcar (lambda (x)
                        (format "1. %s" (org-property-to-link x)))
                      (gen-links-properties directory)))))

(defun gen-prev-next (&optional directory)
  (interactive)
  (unless (cl-find-if (lambda (exclude-file)
                        (file-equal-p buffer-file-name exclude-file))
                      overview-exclude-files)
    (let* ((current-property (gen-org-property buffer-file-name))
           (properties (gen-links-properties directory))
           (index (cl-position current-property properties :test #'equal)))
      (when (eql index nil)
        (error "This org file was not part of this project"))
      (let* ((next (elt properties (min (1- (length properties)) (+ index 1))))
             (prev (elt properties (max 0 (- index 1)))))
        (concat
         "<nav class=\"pagenav\">""\n"
         "<span class=\"pageleft\">"
         (org-property-to-link prev "&larr; ")
         "</span>""\n"
         "<span class=\"pageright\">"
         (org-property-to-link next nil " &rarr;")
         "</span>""\n"
         "</nav>""\n")))))
(defun org-xhtml--build-pagenav (info)
  (when (plist-get info :html-pagenav)
    (gen-prev-next)))

(defun org-xhtml-template (contents info)
  "Return complete document string after HTML conversion.
CONTENTS is the transcoded contents string.  INFO is a plist
holding export options."
  (concat
   (when (and (not (org-html-html5-p info)) (org-html-xhtml-p info))
     (let* ((xml-declaration (plist-get info :html-xml-declaration))
	    (decl (or (and (stringp xml-declaration) xml-declaration)
		      (cdr (assoc (plist-get info :html-extension)
				  xml-declaration))
		      (cdr (assoc "html" xml-declaration))
		      "")))
       (when (not (or (not decl) (string= "" decl)))
	 (format "%s\n"
		 (format decl
			 (or (and org-html-coding-system
				  (fboundp 'coding-system-get)
				  (coding-system-get org-html-coding-system 'mime-charset))
			     "iso-8859-1"))))))
   (org-html-doctype info)
   "\n"
   (concat "<html"
	   (cond ((org-html-xhtml-p info)
		  (format
		   " xmlns=\"http://www.w3.org/1999/xhtml\" lang=\"%s\" xml:lang=\"%s\""
		   (plist-get info :language) (plist-get info :language)))
		 ((org-html-html5-p info)
		  (format " lang=\"%s\"" (plist-get info :language))))
	   ">\n")
   "<head>\n"
   (org-xhtml--build-meta-info info)
   (org-html--build-head info)
   (org-html--build-mathjax-config info)
   "</head>\n"
   "<body>\n"
   (let ((link-up (org-trim (plist-get info :html-link-up)))
	 (link-home (org-trim (plist-get info :html-link-home))))
     (unless (and (string= link-up "") (string= link-home ""))
       (format (plist-get info :html-home/up-format)
	       (or link-up link-home)
	       (or link-home link-up))))
   ;; Preamble.
   (org-xhtml--build-pre/postamble 'preamble info)
   ;; Document contents.
   (let ((div (assq 'content (plist-get info :xhtml-divs))))
     (format "<%s id=\"%s\" class=\"%s\">\n"
             (nth 1 div)
             (nth 2 div)
             (plist-get info :html-content-class)))
   ;; Document title.
   (when (plist-get info :with-title)
     (let ((title (and (plist-get info :with-title)
		       (plist-get info :title)))
	   (subtitle (plist-get info :subtitle))
	   (html5-fancy (org-html--html5-fancy-p info)))
       (when title
	 (format
	  (if html5-fancy
	      "<header>\n<h1 class=\"title\">%s</h1>\n%s</header>\n"
	    "<h1 class=\"title\">%s%s</h1>\n")
	  (org-export-data title info)
	  (if subtitle
	      (format
	       (if html5-fancy
		   "<p class=\"subtitle\" role=\"doc-subtitle\">%s</p>\n"
		 (concat "\n" (org-html-close-tag "br" nil info) "\n"
			 "<span class=\"subtitle\">%s</span>"))
	       (org-export-data subtitle info))
	    "")))))
   (org-xhtml--build-pre/postamble 'entrymeta info)
   contents
   (org-xhtml--build-pagenav info)
   (org-xhtml--build-pre/postamble 'comment info)
   (format "</%s>\n" (nth 1 (assq 'content (plist-get info :xhtml-divs))))
   ;; Postamble.
   (org-xhtml--build-pre/postamble 'postamble info)
   ;; Possibly use the Klipse library live code blocks.
   (when (plist-get info :html-klipsify-src)
     (concat "<script>" (plist-get info :html-klipse-selection-script)
	     "</script><script src=\""
	     org-html-klipse-js
	     "\"></script><link rel=\"stylesheet\" type=\"text/css\" href=\""
	     org-html-klipse-css "\"/>"))
   ;; Closing document.
   "</body>\n</html>"))

;;;###autoload
(defun org-xhtml-publish-to-html (plist filename pub-dir)
  "Publish an org file to HTML.

FILENAME is the filename of the Org file to be published.  PLIST
is the property list for the given project.  PUB-DIR is the
publishing directory.

Return output file name."
  (org-publish-org-to 'xhtml filename
		      (concat (when (> (length org-html-extension) 0) ".")
			      (or (plist-get plist :html-extension)
				  org-html-extension
				  "html"))
		      plist pub-dir))


(provide 'ox-config)
;;; ox-config.el ends here
