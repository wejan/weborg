;; publish.el -- Publish Website using Org mode
;;
(require 'package)
(package-initialize)
(setq package-archives '(("gnu" . "https://mirrors.ustc.edu.cn/elpa/gnu/")
                         ("melpa" . "https://mirrors.ustc.edu.cn/elpa/melpa/")
                         ("nongnu" . "https://mirrors.ustc.edu.cn/elpa/nongnu/")))
(package-refresh-contents)
;(package-install 'htmlize)
(add-to-list 'load-path
	     (concat (file-name-directory (or load-file-name buffer-file-name)) "elisp"))
(require 'org)
(require 'ox-publish)
(require 'ox-rss)
(require 'cl-lib)
(require 'ox-config)
(setq debug-on-error t) 
;; Set default settings
(setq-default org-display-custom-times t)
(setq org-time-stamp-custom-formats 
  '("%Y-%m-%d" . "%Y-%m-%d %A %H:%M"))
(setq org-export-with-inlinetasks nil
      org-export-with-section-numbers nil
      org-export-with-smart-quotes t
      org-export-with-statistics-cookies nil
      org-export-with-toc nil
      org-export-with-tasks nil
      org-export-with-creator nil
      org-export-html-style-include-default nil
      org-export-with-sub-superscripts nil
      org-export-html-head-include-default-style nil
      org-export-html-head-include-scripts nil
      org-export-date-timestamp-format "%Y-%m-%d")
;; HTML settings
(setq org-html-divs '((html-preamble "nav" "top")
                      (content "main" "content")
                      (html-postamble "footer" "postamble")
		      (comment "div" "comment")
		      (entrymeta "div" "entrymeta"))
      org-html-container-element "section"
      org-html-metadata-timestamp-format "%Y-%m-%d"
      org-html-checkbox-type 'html
      org-html-html5-fancy t
      org-html-htmlize-output-type 'css
      org-html-doctype "html5")
(setq make-backup-files nil)
(setq org-src-fontify-natively t)
(setq org-rss-include-post-content t)
(setq org-confirm-babel-evaluate nil)
(setq postamble (with-temp-buffer
                  (insert-file-contents "template/postamble.html")
                  (buffer-string))
      preamble (with-temp-buffer
                  (insert-file-contents "template/preamble.html")
                  (buffer-string))
      comment (with-temp-buffer
                  (insert-file-contents "template/comment.html")
                  (buffer-string))
      header (with-temp-buffer
                  (insert-file-contents "template/head.html")
		  (buffer-string))
      logmeta (with-temp-buffer
                  (insert-file-contents "template/log.org")
                  (buffer-string)))
(setq overview-exclude-files '("index.org")
      sitename "村野集"
)

(defun parent-dir (file)
  "Return the parent directory of FILE."
  (unless (equal "/" file)
    (file-name-directory (directory-file-name file))))
(setq blog-title "村野往事"
      note-title "村野书房"
      blog-root (parent-dir (or load-file-name buffer-file-name)))
(defvar website-url "https://weiqiang.org")
(defvar blog-url "https://weiqiang.org/zh")

(defun project-dir (&optional dir)
  "Get the absolute path of DIR as if it is a directory in BLOG-ROOT."
  (expand-file-name (or dir "") blog-root))

(defun root-link (link)
  "Append LINK to BLOG-ROOT."
  (concat (file-name-as-directory blog-url) link))

(defun posts-rss-feed (title list)
  "Generate a sitemap of posts that is exported as a RSS feed.
TITLE is the title of the RSS feed.  LIST is an internal
representation for the files to include.  PROJECT is the current
project."
  (concat
   "#+TITLE: " title "\n\n"
          (org-list-to-subtree list)))

(defun format-posts-rss-feed-entry (entry _style project)
  "Format ENTRY for the posts RSS feed in PROJECT."
  (let* (
         (title (org-publish-find-title entry project))
	 (author (org-publish-find-property entry :author project 'html))
         (link (concat (file-name-sans-extension entry) ".html"))
	 (description (org-publish-find-property entry :description project 'html))
         (pubdate (format-time-string "%Y-%m-%d" (org-publish-find-date entry project))))
    (format "%s
:properties:
:rss_permalink: %s
:description: %s
:author: %s
:pubdate: %s
:end:\n"
            title
            link
	    description
	    author
            pubdate)))

(defun publish-posts-rss-feed (plist filename dir)
  "Publish PLIST to RSS when FILENAME is rss.org.
DIR is the location of the output."
  (if (equal "rss.org" (file-name-nondirectory filename))
      (org-rss-publish-to-rss plist filename dir)))

;; from https://egh0bww1.com/posts/2023-01-26-26-ox-html-and-ox-publish/
(defun ad-org-html-meta-entry (st)
  (let ((len (length st)))
    (concat (substring st nil (- len 4))
            ">\n")))
(advice-add 'org-html--build-meta-entry :filter-return 'ad-org-html-meta-entry)
;;(advice-remove 'org-html--build-meta-entry 'ad-org-html-meta-entry)

(defun wq/log-index (title list)
  (mapconcat
   'identity
   (list
    (concat "#+TITLE: " title "\n")
    (concat "#+OPTIONS: comment:nil pagenav:nil entrymeta:nil
#+DATE: [2008-11-18]
#+AUTHOR: 落为匠
#+DESCRIPTION: 这里纪录一些个人感悟、生活经历等等，拒绝道听途说。
#+KEYWORDS: blog, 博客, 村野集, 桑下居, 围墙里\n")
    (concat "#+begin_export html
<div class=\"meta\">
<p>旧日往事存档。2008年-2020年，基本不再更新。</p>
</div>
#+end_export")
     (org-list-to-subtree list nil '(:istart "")))
   "\n"
   ))

(defun wq/log-sitemap-format-entry (entry style project)
  (format "
    #+HTML: <section class=\"post-entry\" id=\"%s\">
    #+HTML: <span class=\"pubdate\">%s</span>
    #+HTML: <h2 class=\"article-title\"><a href=\"/log/%s\">%s</a></h2>
    #+HTML: <div class=\"description\">%s</div>
    #+HTML: </section>"
	  (file-name-sans-extension entry)
	  (format-time-string "%Y-%m-%d" (org-publish-find-date entry project))
	  (concat (file-name-sans-extension entry) ".html")
          (org-publish-find-title entry project)
	  (org-publish-find-property entry :description project 'html)))

(defun wq/zh-sitemap-format-entry (entry style project)
  (format "
    #+HTML: <div class=\"entry\" id=\"%s\">
    #+HTML: <span class=\"entry-title\"><a href=\"/zh/%s\" title=\"%s\">%s</a></span><time class=\"date\">%s</time>
    #+HTML: </div>"
	  (file-name-sans-extension entry)
	  (concat (file-name-sans-extension entry) ".html")
	  (org-publish-find-property entry :description project 'html)
          (org-publish-find-title entry project)
	  (format-time-string "%Y-%m-%d" (org-publish-find-date entry project))))

(defun wq/blog-index (title list)
  (mapconcat
   'identity
   (list
    (concat "#+TITLE: " title "\n" logmeta)
     (org-list-to-subtree list nil '(:istart "")))
   "\n"
   ))

(defun wq/note-index (title list)
  (mapconcat
   'identity
   (list
    (concat "#+TITLE: " title)
     (org-list-to-subtree list nil '(:istart "- ")))
   "\n"
   ))

(setq org-export-global-macros
      '(("datetime" . "@@html:<time class=\"timestamp\">$1</time>@@")))

(defun wq/sitemap-format-entry (entry style project)
  "Format sitemap entry for ENTRY STYLE PROJECT."
  (cond ((not (directory-name-p entry))
         (format "%s - [[file:%s][%s]]"
		 (format-time-string "%Y-%m-%d" (org-publish-find-date entry project))
                 entry
                 (org-publish-find-title entry project)
		 ))
        ((eq style 'list)
         ;; Return only last subdir.
         (file-name-nondirectory (directory-file-name entry)))
        (t entry)))

;; Set project
(setq org-publish-project-alist
      `(;("log"
	 ;:base-directory ,(project-dir "org/log")
	 ;:base-extension "org"
	 ;:exclude ,(regexp-opt '("index.org"))
	 ;:publishing-directory ,(project-dir "public/log")
         ;:publishing-function ignore
	 ;:recursive t
	 ;:auto-sitemap t
	 ;:sitemap-title "往事"
	 ;:sitemap-filename "index.org"
	 ;:sitemap-function wq/log-index
	 ;:sitemap-format-entry wq/log-sitemap-format-entry
	 ;:sitemap-style list
	 ;:sitemap-sort-files anti-chronologically
	 ;)
	("zh"
	 :base-directory ,(project-dir "org/zh")
	 :base-extension "org"
	 :publishing-directory ,(project-dir "public/zh")
         :publishing-function ignore
	 :recursive t
	 :auto-sitemap t
	 :sitemap-title "网志"
	 :sitemap-filename "index.org"
	 :sitemap-function wq/blog-index
	 :sitemap-format-entry wq/zh-sitemap-format-entry
	 :sitemap-style list
	 :sitemap-sort-files anti-chronologically
	 )
	("pages"
	 :base-directory ,(project-dir "org")
	 :base-extension "org"
	 :exclude ,(regexp-opt '("draft" "log/" "rss.org"))
	 :publishing-directory ,(project-dir "public")
         :publishing-function org-xhtml-publish-to-html
	 :recursive t
	 :htmlized-source t
	 :headline-level 4
	 :html-head-include-default-style nil
	 :html-head-include-scripts nil
	 :html-link-home ,website-url
	 :html-home/up-format ""
	 :html-head ,header
         :html-preamble ,preamble
         :html-postamble ,postamble
	 :comment ,comment
	 :pagenav nil
	 :entrymeta "<span class=\"date\">%d</span>"
	 :auto-sitemap nil
	 )
        ("assets"
         :base-directory ,(project-dir "assets")
         :base-extension any
         :publishing-directory ,(project-dir "public")
         :publishing-function org-publish-attachment
         :recursive t
	 :exclude ,(regexp-opt '("public" ".*\.org")))
	("images"
         :base-directory ,(project-dir "org/images")
         :base-extension any
         :publishing-directory ,(project-dir "public/images")
         :publishing-function org-publish-attachment
         :recursive t)
        ("rss"
         :base-directory ,(project-dir "org")
         :base-extension "org"
         :recursive t
       	 :exclude ,(regexp-opt '("index.org" "rss.org" "draft" "404.org" "about.org" "sitelog.org" "links.org" "comment.org" "now.org" "memos.org" "copyright.org" "zh/index.org" "log/" "topic" "zhijiao"))
         :publishing-directory ,(project-dir "public")
         :publishing-function publish-posts-rss-feed
         :html-link-home ,website-url
         :html-link-use-abs-url t
	 :rss-extension "xml"
	 :rss-image-url ,(concat website-url "/images/feed.png")
	 :email "lu@weiqiang.org"
	 :author "落为匠"
	 :description "落为匠的村野往事。"
	 :language "zh-CN"
         :auto-sitemap t
	 :sitemap-filename "rss.org"
	 :sitemap-title ,blog-title
	 :sitemap-style list
	 :sitemap-sort-files anti-chronologically
	 :sitemap-function posts-rss-feed
	 :sitemap-format-entry format-posts-rss-feed-entry
	 )
        ("website" :components ("pages" "assets" "images" "rss"))))
(provide 'publish)
